const {src, dest, series} = require('gulp');
const babel = require('gulp-babel');
 

function scripts(){
    return src('src/js/*.js')
    .pipe(babel({
        presets: ['@babel/env']
    }))
    .pipe(dest('build/js/'))
}

exports.babel = scripts;